package com.simpragma.javaassignment;


import java.io.*;
import static java.lang.Math.*;
public class Narcissistic {boolean check(int n)
{
    // count the number of digits
    int l = countDigit(n);
    int dup = n;
    int sum = 0;
  
    // calculates the sum of 
    //digits raised to power
    while(dup > 0) 
	    {
	        sum += pow(dup % 10, l);
	        dup /= 10;
	    }
	  
	    	return (n == sum);
		}



	int countDigit(int n)
	{
	    if (n == 0)
	        return 0;
	  
	    return 1 + countDigit(n / 10);
	}
}
